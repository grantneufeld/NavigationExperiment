import SwiftUI

@Observable
class ExpenseCollection: Equatable, Hashable, MutableCollection, RandomAccessCollection, Sequence {
    typealias Element = ExpenseEntity

    var collection: [Element] = []
    var startIndex: Int { self.collection.startIndex }
    var endIndex: Int { self.collection.endIndex }
    var count: Int { self.collection.count }

    init(_ newCount: Int = 0) {
        for index in 0..<newCount {
            self.append(Element(amount: Decimal(index)))
        }
    }

    // MARK: - Equatable

    static func == (lhs: ExpenseCollection, rhs: ExpenseCollection) -> Bool {
        lhs.collection == rhs.collection
    }

    // MARK: - Collection

    subscript(_ index: Int) -> Element {
        get { self.collection[index] }
        set(newElement) { self.collection[index] = newElement }
    }

    func binding(_ index: Int) -> Binding<Element> {
        Binding<Element>(
            get: { self[index] },
            set: { newValue in self[index] = newValue }
        )
    }

    func append(_ newElement: Element) {
        self.collection.append(newElement)
    }

    // MARK: - Hashable

    func hash(into hasher: inout Hasher) {
        hasher.combine(self.collection)
    }
}
