import Foundation

/// Doing this instead of a directory so that it can be Bindable and iterable for a `List` or `ForEach`.
class DatedExpenseCollection: Equatable, Hashable, Identifiable {
    var date: Date
    var expenses: ExpenseCollection

    init(date: Date = Date(), expenses: ExpenseCollection? = nil, sampleCount: Int = 0) {
        self.date = date
        self.expenses = expenses ?? ExpenseCollection(sampleCount)
    }

    static func == (lhs: DatedExpenseCollection, rhs: DatedExpenseCollection) -> Bool {
        lhs.date == rhs.date && lhs.expenses == rhs.expenses
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(self.date)
        hasher.combine(self.expenses)
    }
}
