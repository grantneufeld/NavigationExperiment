import Foundation

class ExpenseEntity: Equatable, Hashable, Identifiable {
    var amount: Decimal = 0

    var uuid = UUID()
    var id: UUID { self.uuid }

    init(amount: Decimal) {
        self.amount = amount
    }

    // MARK: - Equatable

    static func == (lhs: ExpenseEntity, rhs: ExpenseEntity) -> Bool {
        lhs.amount == rhs.amount
    }

    // MARK: - Hashable

    func hash(into hasher: inout Hasher) {
        hasher.combine(self.amount)
    }
}
