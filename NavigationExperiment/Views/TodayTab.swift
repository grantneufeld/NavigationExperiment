import SwiftUI

struct TodayTab: View {
    var body: some View {
        Text("The Today View.")
            .font(.title)
    }
}

#Preview {
    TodayTab()
}
