import SwiftUI

struct HistoryTab: View {
    /// Some sample values for testing.
    /// In a real application, this would be built by fetching from CoreData, or whatever…
    static var datedExpenses = [DatedExpenseCollection(sampleCount: 3)]

    @State var historyDates = Self.datedExpenses

    var body: some View {
        NavigationStack {
            VStack {
                List($historyDates, id: \.self) { historyDate in
                    NavigationLink(value: historyDate.wrappedValue) {
                        Text("\(historyDate.wrappedValue.date.ISO8601Format()): \(historyDate.wrappedValue.expenses.count) expenses")
                    }
                }
            }
            .navigationDestination(for: DatedExpenseCollection.self) { datedExpenses in
                ExpensesScene(datedExpenses: self.boundDatedExpenses(datedExpenses))
            }
        }
        .navigationTitle("History")
    }

    /// Basically, fake a Binding because SwiftUI binding stuff is annoying 😡
    /// - Parameter datedExpenses: The value to wrap in a Binding.
    /// - Returns: The given value wrapped in a Binding.
    private func boundDatedExpenses(_ datedExpenses: DatedExpenseCollection) -> Binding<DatedExpenseCollection> {
        .init {
            datedExpenses
        } set: { _ in
            // ignore?
        }
    }
}

#Preview {
    HistoryTab()
}
