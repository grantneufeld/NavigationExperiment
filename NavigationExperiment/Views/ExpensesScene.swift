import SwiftUI

struct ExpensesScene: View {
    @Binding var datedExpenses: DatedExpenseCollection
    // @State private var expenses = ExpenseCollection(3)
    @State private var selected: ExpenseEntity?

    var body: some View {
        NavigationStack {
            List(datedExpenses.expenses, rowContent: { expense in
                NavigationLink(value: expense) {
                    Text("Expense: \(expense.amount.formatted())")
                }
            })
            .navigationDestination(for: ExpenseEntity.self) { expense in
                ExpenseScene(expense: fakeBinding(expense))
            }
        }
        .navigationTitle("Expenses")
    }

    /// Basically, fake a Binding because SwiftUI binding stuff is annoying 😡
    /// - Parameter datedExpenses: The value to wrap in a Binding.
    /// - Returns: The given value wrapped in a Binding.
    private func fakeBinding<T>(_ itemToBind: T) -> Binding<T> {
        .init {
            itemToBind
        } set: { _ in
            // ignore?
        }
    }
}

#Preview {
    @State var datedExpenses = DatedExpenseCollection(sampleCount: 3)
    return ExpensesScene(datedExpenses: $datedExpenses)
}
