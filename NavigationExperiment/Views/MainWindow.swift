import SwiftUI

enum MainTabs {
    case today
    case history
    case settings
}

struct MainWindow: View {
    @State private var selectedTab = MainTabs.today

    var body: some View {
        TabView(selection: $selectedTab) {
            TodayTab()
                .tabItem {
                    Label("Today", systemImage: "star")
                }
            HistoryTab()
                .tabItem {
                    Label("History", systemImage: "calendar")
                }
            SettingsTab()
                .tabItem {
                    Label("Settings", systemImage: "gear")
                }
        }
        .navigationTitle("My App")
    }
}

#Preview {
    MainWindow()
}
