import SwiftUI

struct ExpenseScene: View {
    @Binding var expense: ExpenseEntity

    var body: some View {
        Text("Expense for \(self.expense.amount.formatted())")
            .navigationTitle("Expense")
    }
}

#Preview {
    @State var expense = ExpenseEntity(amount: 12.34)
    return ExpenseScene(expense: $expense)
}
