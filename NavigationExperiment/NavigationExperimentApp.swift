import SwiftUI

@main
struct NavigationExperimentApp: App {
    var body: some Scene {
        WindowGroup {
            MainWindow()
        }
    }
}
